﻿using UnityEngine;
using System.Collections;
using MidiJack;
using System;
using UnityEngine.Events;
using System.Collections.Generic;

[Serializable]
public class MidiKeyActiveEvent : UnityEvent<float> { }

[Serializable]
public class MidiEvent
{
    public int key;
    public UnityEvent onDown;
    public UnityEvent onUp;
    public MidiKeyActiveEvent onActive;
}

public class MidiEventTrigger : MonoBehaviour {
    [SerializeField] private List<MidiEvent> eventList;

	void Update()
    {
        for(int i = 0; i < eventList.Count; i++)
        {
            var evt = eventList[i];
            if (MidiMaster.GetKeyDown(evt.key))
            {
                evt.onDown.Invoke();
            }
            if (MidiMaster.GetKeyUp(evt.key))
            {
                evt.onUp.Invoke();
            }
            evt.onActive.Invoke(MidiMaster.GetKey(evt.key));
        }
    }
}
